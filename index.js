const express = require("express");
const path = require("path");

const app = express();

const port = 8000;

app.use(express.static(__dirname + "/views/Course 365"));

app.get("/course", (req, res) =>{
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/views/Course 365/index.html"))

})

app.listen(port, () => {
    console.log(`Đang chạy cổng ${port}`)
})